<?php

/**
 * @file
 * This is a module which takes headings and converts headings into tabs
 *
 * This module was built from the filter example on the Drupal website.
 */

/**
 * Implementation of hook_help().
 *
 */
function tabsauto_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      // This description is shown in the listing at admin/modules.
      return t('Automatically splits pages into tabs where ever there is a &lt;H1&gt; heading (or at other configured element).');
  }
}

/**
 * Implementation of hook_filter_tips().
 */
function tabsauto_filter_tips($format, $long = FALSE) {
  if ($long) {
    return t('Every instance of '. variable_get("tabsauto_element", 'H1') .' tags will be displayed as a seperate tab.');
  }
  else {
    return t('Automatically create tabs from node content.');
  }
}

/**
 * Implementation of hook_filter().
 *
 * The bulk of filtering work is done here.
 */
function tabsauto_filter($op, $delta = 0, $format = -1, $text = '') {
  if ($op == 'list') {
    return array(
      0 => t('Tabs-Auto')
    );
  }
  // We currently only define one filter, so we don't need to switch on $delta
  switch ($op) {
    // Admin interface description
    case 'description':
      return t('Automatically create tabs from node content.');

    // Ensure that the content is regenerated on every preview
    case 'no cache':
      return TRUE;

    case 'prepare':
      return $text;

    // Here, we pull out the required information and add the DIV
    case 'process':
      $path = drupal_get_path('module', 'tabsauto');
      drupal_add_css($path.'/tabsauto.css');

      // load Jquery UI Tabs
      jquery_ui_add(array('ui.tabs'));

      drupal_set_html_head('<script>$(document).ready(function(){ $("#tabsauto > ul").tabs(); });</script>');

      // wrap DIV wrappers around each tab-element section ready to transform sections into tabs
      $matches = array();
      $element = variable_get("tabsauto_element", 'H1');

      // split into sections at each instance of "element"
      preg_match_all("/(.+?(?=<". $element ."|$))/is", $text, $matches, PREG_PATTERN_ORDER); 
      $count = 0;
      $anchors = array();

      // process each tab section
      foreach ($matches[0] as $match) {
        preg_match("/(<". $element ."[^>]*>)(.+?)<\/". $element .">(.+)/is", $match, $sect); 
        
        if ($sect[2]) { 
           $sectiontitle = $sect[2]; 
           $section = $sect[3];
           if (variable_get("tabsauto_include", 0)) {
             $section = '<'. $element .'>'. $sectiontitle .'</'. $element .'>'. $section;
           }
           preg_match("/(<". $element ."[^>]*>)(.+?)<\/". $element .">(.+)/is", $match, $sect); 

           // check for existing id and use that if found else use section-title
           if (preg_match('/id="(.*?)"/i', $sect[1], $existing_id)) {
             $anchor = $existing_id[1]; 
           }
           else {
             $anchor = $sectiontitle; 
           }

        } 
        else { 
           $sectiontitle = variable_get("tabsauto_1st_tab", t('Intro')); 
           $section = $match;
           $anchor = $sectiontitle;
        }

        // sanitize ID text & remove leading digits (invalid on IDs)
        $anchor = preg_replace("/&amp;/", "", strip_tags($anchor));
        $anchor = preg_replace("/[^A-Za-z0-9]/", "", $anchor);
        $anchor = preg_replace("/^[0-9]+/", "", $anchor);

        $tabbed = $tabbed.'<div id="'.$anchor.'">'.$section.'</div>';        
        $tablist = $tablist.'<li><a href="#'. $anchor .'">'. $sectiontitle .'</a></li>';
        $count++;
      }
      $text = '<div id="tabsauto" class="flora"><ul>'.$tablist.'</ul>'.$tabbed.'</div>';
      return $text;

   case 'settings':
     $form['tabsauto'] = array(
       '#type' => 'fieldset',
       '#title' => t('Tabs-Auto'),
       '#collapsible' => TRUE,
       '#collapsed' => FALSE,
     );
    $form['tabsauto']["tabsauto_element"] = array(
       '#type'    => 'textfield',
       '#title'   => t('Tab seperator element'),
       '#default_value' => variable_get("tabsauto_element", 'H1'),
       '#description'  => t('Select which HTML or markup tag to use as the tab location marker. This must be HTML style &lt;element-name&gt;title text&lt;/element-name&gt; such as H1 which contains the tabs title and preceeds the content to be tabbed.'),
     );
    $form['tabsauto']["tabsauto_1st_tab"] = array(
       '#type'    => 'textfield',
       '#title'   => t('Default Tab title'),
       '#default_value' => variable_get("tabsauto_1st_tab", t('Intro')),
       '#description'  => t('Tab name when the content starts without the selected header element. "Intro" by default.'),
     );
    $form['tabsauto']["tabsauto_include"] = array(
       '#type' => 'checkbox',
       '#title'   => t('Include seperator'),
       '#default_value' => variable_get("tabsauto_include", 0),
       '#description'  => t("Include the original 'Tab seperator element' and in it's content at the start of the tabed page. Normally the contents of the seperator are used to provide the tab title only."),
     );
    return $form;
  }
}

