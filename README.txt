TabsAuto Module v1
===========================
By John Bryan - www.ALT2.com Tel UK 08700 3456 31

This module will scan a node and split it into tab sections at each <H1> heading element using the heading title as the tab's title (can be set to split at other HTML/XML elements instead). Any text before the first heading element is encountered will be listed under a tab entitled "Intro".


Primary aim is to be able to split existing nodes into tabs without re-editing and to allow "dumb users" to create tabbed node content without any extra tasks or commands being required - if they can select "Heading1" in the WYSIWYG editor or use <H1> in HTML then they can create tabs. 



1. Installation
---------------

a. install & enable the "JQuery UI" module (http://drupal.org/project/jquery_ui)
   NOTE: Look at the README.txt for this module as it requires some files to 
         be downloaded and installed in to it's module directory!


b. place the tabsauto module folder in to your modules directory. 


c. enable Tabs-Auto at admin/modules screen


d. enable the filter itself at admin/input formats, and configure what HTML element to split your nodes at (defaults to "H1"). This can be any HTML / XML style element that preceeds the section contents and contains the required section title.


e. Add the Tabs-Auto input filter to your desired input types.



2. Use
------
Use H1 heading entries in node content to define the location and name off tabs. The filter can be configured to split at elements other than H1 such as H3 or even XML elements etc. 


Tabs-Auto configuration is availble via:-

  "Admin/Site Configuration/Input Formats" (navigation menu)
  Click "configure" for the Tabs-Auto enabled 'Input Format'
  Click on the "Configure" tab
  Look under the "Tabs-Auto" section

Configuration Options are:-

"Tab seperator element:" 
To change the HTML or XML element used as a splitting point and to supply the tab titles. Do not enter the <> brackets. {Default = H1 }

"Default Tab title:"
The title to use for the initial tab when the document/node does not start with the defined tab-seperator. {Default = Intro }

"Include seperator:"
By default the defined tab-seperator (e.g. <h1>Section One</h1>) is converted into a titled tab and does not appear in the main content area. Enable this check box to also display the tab-seperator-element at the top of the tabbed page. { Default = Ommit }


3. Removal / Deinstallation
---------------------------
No database tables are created. To remove the module, just disable and delete the module.



4. Notes
--------


a) Any ID attributes in the original tab-seperator-element are removed and applied to the Tab to allow retention of URL link anchors. Otherwise the contents of the tab-seperator-element are cleaned and used as an ID/anchor. 

b) tab-seperator-elements must conform to the following relationship with the intended tab content:-

  <element>Tab Title</element>Tab page content including any html tags etc.

And not:-

  <element>Tab Title. Tab page content including any html tags etc.</element>

In future releases a the use of encosing elements similar to the 2nd example will be allowable.


5. To Do
--------

 - Handle when tab-seperator is enclosed by, or encloses, a <A ...></A>.

 - Advanced option to use an alternative attribute than ID as the anchor name.

 - Advanced option to use an attribute as the section/tab title.

 - Advanced option to identify sections by a wrapper element instead of a prefix element.

 - Use DOM instead or RegEx for searching and splitting nodes - for scalability. 



A secondary aim to be implimented in future development is to be able to flexible cope with not just normal nodes but also XML constructs such as:-


  <BOOK title="The Time Machine">
     <Author>H.G.Wells</Author>
     <description>An all time clasic.<description>
  </BOOK>

  <BOOK title="20,000 leagues">
     <Author>Jules Verne</Author>
     <description>Epic under sea adventure.<description>
  </BOOK>


And split the list/document to produce:-


  The Time Machine | 20,000 Leagues |
  ---------------------------------------------
  An all time clasic.


